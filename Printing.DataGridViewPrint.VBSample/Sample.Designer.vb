﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Sample
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Sample))
        Me.chkFitColumns = New System.Windows.Forms.CheckBox
        Me.chkCenter = New System.Windows.Forms.CheckBox
        Me.btnPreview = New System.Windows.Forms.Button
        Me.btnPrint = New System.Windows.Forms.Button
        Me.GridView = New System.Windows.Forms.DataGridView
        Me.printDialog = New System.Windows.Forms.PrintDialog
        Me.printDocument = New System.Drawing.Printing.PrintDocument
        Me.printPreviewDialog = New System.Windows.Forms.PrintPreviewDialog
        Me.chkLevelByLevel = New System.Windows.Forms.CheckBox
        Me.CustomerIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CompanyNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ContactNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ContactTitleDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AddressDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CityDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.RegionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PostalCodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CountryDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PhoneDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FaxDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Customers2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CustomersDataSet = New Printing.DataGridViewPrint.VBSample.CustomersDataSet
        Me.Customers2TableAdapter = New Printing.DataGridViewPrint.VBSample.CustomersDataSetTableAdapters.Customers2TableAdapter
        Me.chkSelectedRows = New System.Windows.Forms.CheckBox
        CType(Me.GridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Customers2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CustomersDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'chkFitColumns
        '
        Me.chkFitColumns.AutoSize = True
        Me.chkFitColumns.Location = New System.Drawing.Point(311, 11)
        Me.chkFitColumns.Name = "chkFitColumns"
        Me.chkFitColumns.Size = New System.Drawing.Size(125, 17)
        Me.chkFitColumns.TabIndex = 16
        Me.chkFitColumns.Text = "Fit Columns To sheet"
        Me.chkFitColumns.UseVisualStyleBackColor = True
        '
        'chkCenter
        '
        Me.chkCenter.AutoSize = True
        Me.chkCenter.Checked = True
        Me.chkCenter.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkCenter.Location = New System.Drawing.Point(202, 11)
        Me.chkCenter.Name = "chkCenter"
        Me.chkCenter.Size = New System.Drawing.Size(103, 17)
        Me.chkCenter.TabIndex = 15
        Me.chkCenter.Text = "Center Partitions"
        Me.chkCenter.UseVisualStyleBackColor = True
        '
        'btnPreview
        '
        Me.btnPreview.Location = New System.Drawing.Point(107, 7)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(89, 23)
        Me.btnPreview.TabIndex = 14
        Me.btnPreview.Text = "Print Pre&view"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Location = New System.Drawing.Point(12, 7)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(89, 23)
        Me.btnPrint.TabIndex = 13
        Me.btnPrint.Text = "&Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'GridView
        '
        Me.GridView.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridView.AutoGenerateColumns = False
        Me.GridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.GridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CustomerIDDataGridViewTextBoxColumn, Me.CompanyNameDataGridViewTextBoxColumn, Me.ContactNameDataGridViewTextBoxColumn, Me.ContactTitleDataGridViewTextBoxColumn, Me.AddressDataGridViewTextBoxColumn, Me.CityDataGridViewTextBoxColumn, Me.RegionDataGridViewTextBoxColumn, Me.PostalCodeDataGridViewTextBoxColumn, Me.CountryDataGridViewTextBoxColumn, Me.PhoneDataGridViewTextBoxColumn, Me.FaxDataGridViewTextBoxColumn})
        Me.GridView.DataSource = Me.Customers2BindingSource
        Me.GridView.Location = New System.Drawing.Point(1, 36)
        Me.GridView.Name = "GridView"
        Me.GridView.Size = New System.Drawing.Size(775, 348)
        Me.GridView.TabIndex = 12
        '
        'printDialog
        '
        Me.printDialog.Document = Me.printDocument
        Me.printDialog.ShowNetwork = False
        Me.printDialog.UseEXDialog = True
        '
        'printDocument
        '
        Me.printDocument.DocumentName = "Customers Report"
        '
        'printPreviewDialog
        '
        Me.printPreviewDialog.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.printPreviewDialog.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.printPreviewDialog.ClientSize = New System.Drawing.Size(400, 300)
        Me.printPreviewDialog.Document = Me.printDocument
        Me.printPreviewDialog.Enabled = True
        Me.printPreviewDialog.Icon = CType(resources.GetObject("printPreviewDialog.Icon"), System.Drawing.Icon)
        Me.printPreviewDialog.Name = "printPreviewDialog"
        Me.printPreviewDialog.Visible = False
        '
        'chkLevelByLevel
        '
        Me.chkLevelByLevel.AutoSize = True
        Me.chkLevelByLevel.Checked = True
        Me.chkLevelByLevel.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkLevelByLevel.Location = New System.Drawing.Point(442, 11)
        Me.chkLevelByLevel.Name = "chkLevelByLevel"
        Me.chkLevelByLevel.Size = New System.Drawing.Size(125, 17)
        Me.chkLevelByLevel.TabIndex = 17
        Me.chkLevelByLevel.Text = "Print Levels by levels"
        Me.chkLevelByLevel.UseVisualStyleBackColor = True
        '
        'CustomerIDDataGridViewTextBoxColumn
        '
        Me.CustomerIDDataGridViewTextBoxColumn.DataPropertyName = "CustomerID"
        Me.CustomerIDDataGridViewTextBoxColumn.HeaderText = "CustomerID"
        Me.CustomerIDDataGridViewTextBoxColumn.Name = "CustomerIDDataGridViewTextBoxColumn"
        Me.CustomerIDDataGridViewTextBoxColumn.Width = 87
        '
        'CompanyNameDataGridViewTextBoxColumn
        '
        Me.CompanyNameDataGridViewTextBoxColumn.DataPropertyName = "CompanyName"
        Me.CompanyNameDataGridViewTextBoxColumn.HeaderText = "CompanyName"
        Me.CompanyNameDataGridViewTextBoxColumn.Name = "CompanyNameDataGridViewTextBoxColumn"
        Me.CompanyNameDataGridViewTextBoxColumn.Width = 104
        '
        'ContactNameDataGridViewTextBoxColumn
        '
        Me.ContactNameDataGridViewTextBoxColumn.DataPropertyName = "ContactName"
        Me.ContactNameDataGridViewTextBoxColumn.HeaderText = "ContactName"
        Me.ContactNameDataGridViewTextBoxColumn.Name = "ContactNameDataGridViewTextBoxColumn"
        Me.ContactNameDataGridViewTextBoxColumn.Width = 97
        '
        'ContactTitleDataGridViewTextBoxColumn
        '
        Me.ContactTitleDataGridViewTextBoxColumn.DataPropertyName = "ContactTitle"
        Me.ContactTitleDataGridViewTextBoxColumn.HeaderText = "ContactTitle"
        Me.ContactTitleDataGridViewTextBoxColumn.Name = "ContactTitleDataGridViewTextBoxColumn"
        Me.ContactTitleDataGridViewTextBoxColumn.Width = 89
        '
        'AddressDataGridViewTextBoxColumn
        '
        Me.AddressDataGridViewTextBoxColumn.DataPropertyName = "Address"
        Me.AddressDataGridViewTextBoxColumn.HeaderText = "Address"
        Me.AddressDataGridViewTextBoxColumn.Name = "AddressDataGridViewTextBoxColumn"
        Me.AddressDataGridViewTextBoxColumn.Width = 70
        '
        'CityDataGridViewTextBoxColumn
        '
        Me.CityDataGridViewTextBoxColumn.DataPropertyName = "City"
        Me.CityDataGridViewTextBoxColumn.HeaderText = "City"
        Me.CityDataGridViewTextBoxColumn.Name = "CityDataGridViewTextBoxColumn"
        Me.CityDataGridViewTextBoxColumn.Width = 49
        '
        'RegionDataGridViewTextBoxColumn
        '
        Me.RegionDataGridViewTextBoxColumn.DataPropertyName = "Region"
        Me.RegionDataGridViewTextBoxColumn.HeaderText = "Region"
        Me.RegionDataGridViewTextBoxColumn.Name = "RegionDataGridViewTextBoxColumn"
        Me.RegionDataGridViewTextBoxColumn.Width = 66
        '
        'PostalCodeDataGridViewTextBoxColumn
        '
        Me.PostalCodeDataGridViewTextBoxColumn.DataPropertyName = "PostalCode"
        Me.PostalCodeDataGridViewTextBoxColumn.HeaderText = "PostalCode"
        Me.PostalCodeDataGridViewTextBoxColumn.Name = "PostalCodeDataGridViewTextBoxColumn"
        Me.PostalCodeDataGridViewTextBoxColumn.Width = 86
        '
        'CountryDataGridViewTextBoxColumn
        '
        Me.CountryDataGridViewTextBoxColumn.DataPropertyName = "Country"
        Me.CountryDataGridViewTextBoxColumn.HeaderText = "Country"
        Me.CountryDataGridViewTextBoxColumn.Name = "CountryDataGridViewTextBoxColumn"
        Me.CountryDataGridViewTextBoxColumn.Width = 68
        '
        'PhoneDataGridViewTextBoxColumn
        '
        Me.PhoneDataGridViewTextBoxColumn.DataPropertyName = "Phone"
        Me.PhoneDataGridViewTextBoxColumn.HeaderText = "Phone"
        Me.PhoneDataGridViewTextBoxColumn.Name = "PhoneDataGridViewTextBoxColumn"
        Me.PhoneDataGridViewTextBoxColumn.Width = 63
        '
        'FaxDataGridViewTextBoxColumn
        '
        Me.FaxDataGridViewTextBoxColumn.DataPropertyName = "Fax"
        Me.FaxDataGridViewTextBoxColumn.HeaderText = "Fax"
        Me.FaxDataGridViewTextBoxColumn.Name = "FaxDataGridViewTextBoxColumn"
        Me.FaxDataGridViewTextBoxColumn.Width = 49
        '
        'Customers2BindingSource
        '
        Me.Customers2BindingSource.DataMember = "Customers2"
        Me.Customers2BindingSource.DataSource = Me.CustomersDataSet
        '
        'CustomersDataSet
        '
        Me.CustomersDataSet.DataSetName = "CustomersDataSet"
        Me.CustomersDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Customers2TableAdapter
        '
        Me.Customers2TableAdapter.ClearBeforeFill = True
        '
        'chkSelectedRows
        '
        Me.chkSelectedRows.AutoSize = True
        Me.chkSelectedRows.Enabled = False
        Me.chkSelectedRows.Location = New System.Drawing.Point(573, 11)
        Me.chkSelectedRows.Name = "chkSelectedRows"
        Me.chkSelectedRows.Size = New System.Drawing.Size(115, 17)
        Me.chkSelectedRows.TabIndex = 18
        Me.chkSelectedRows.Text = "Print selected rows"
        Me.chkSelectedRows.UseVisualStyleBackColor = True
        '
        'Sample
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(777, 384)
        Me.Controls.Add(Me.chkSelectedRows)
        Me.Controls.Add(Me.chkLevelByLevel)
        Me.Controls.Add(Me.chkFitColumns)
        Me.Controls.Add(Me.chkCenter)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.GridView)
        Me.Name = "Sample"
        Me.Text = "GridDrawer Sample"
        CType(Me.GridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Customers2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CustomersDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents chkFitColumns As System.Windows.Forms.CheckBox
    Private WithEvents chkCenter As System.Windows.Forms.CheckBox
    Private WithEvents btnPreview As System.Windows.Forms.Button
    Private WithEvents btnPrint As System.Windows.Forms.Button
    Private WithEvents GridView As System.Windows.Forms.DataGridView
    Friend WithEvents CustomersDataSet As Printing.DataGridViewPrint.VBSample.CustomersDataSet
    Friend WithEvents Customers2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Customers2TableAdapter As Printing.DataGridViewPrint.VBSample.CustomersDataSetTableAdapters.Customers2TableAdapter
    Friend WithEvents CustomerIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CompanyNameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ContactNameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ContactTitleDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AddressDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CityDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RegionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PostalCodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CountryDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PhoneDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FaxDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Private WithEvents printDialog As System.Windows.Forms.PrintDialog
    Private WithEvents printDocument As System.Drawing.Printing.PrintDocument
    Private WithEvents printPreviewDialog As System.Windows.Forms.PrintPreviewDialog
    Private WithEvents chkLevelByLevel As System.Windows.Forms.CheckBox
    Private WithEvents chkSelectedRows As System.Windows.Forms.CheckBox

End Class
