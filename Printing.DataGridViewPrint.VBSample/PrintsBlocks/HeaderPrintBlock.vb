﻿Namespace PrintsBlocks

    Public Class HeaderPrintBlock : Inherits PrintBlock
    Dim imgHeight As Double = 78

        Public Overrides Sub Draw(ByVal g As System.Drawing.Graphics, ByVal codes As System.Collections.Generic.Dictionary(Of CodeEnum, String))
            Dim units As GraphicsUnit = GraphicsUnit.Pixel

            Dim rec As RectangleF = My.Resources.logo.GetBounds(units)


            Dim scale As Double = imgHeight / rec.Height
            g.DrawImage(My.Resources.logo, New RectangleF(Rectangle.X, Rectangle.Y, rec.Width * scale, imgHeight))

        End Sub

        Public Overrides Function GetSize(ByVal g As System.Drawing.Graphics, ByVal metrics As DocumentMetrics) As System.Drawing.SizeF
            Return New SizeF(metrics.PrintAbleWidth, imgHeight + 2)
        End Function
  End Class

End Namespace