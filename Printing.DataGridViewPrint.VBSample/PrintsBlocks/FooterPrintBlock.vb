﻿Namespace PrintsBlocks

    Public Class FooterPrintBlock : Inherits PrintBlock

        Dim font As Font = New Font("Tahoma", 9, GraphicsUnit.Point)

        Public Overrides Sub Draw(ByVal g As System.Drawing.Graphics, ByVal codes As System.Collections.Generic.Dictionary(Of CodeEnum, String))
            Dim format As StringFormat = New StringFormat()
            format.Trimming = StringTrimming.Word
            format.FormatFlags = StringFormatFlags.NoWrap
            format.Alignment = StringAlignment.Far

            g.DrawString(String.Format("Page {0} Of {1}", codes(CodeEnum.SheetNumber), codes(CodeEnum.SheetsCount)), _
                         font, _
                         New SolidBrush(Color.Black), _
                         Rectangle, _
                         format)

        End Sub

        Public Overrides Function GetSize(ByVal g As System.Drawing.Graphics, ByVal metrics As DocumentMetrics) As System.Drawing.SizeF
            Return g.MeasureString("Page X Of Y", font)
        End Function

    End Class

End Namespace
