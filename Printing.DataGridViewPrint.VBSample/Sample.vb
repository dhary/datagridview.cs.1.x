﻿Public Class Sample

    Dim PrintProvider As Tools.PrintingDataGridViewProvider = Nothing


    Private Sub Sample_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'CustomersDataSet.Customers2' table. You can move, or remove it, as needed.
        Me.Customers2TableAdapter.Fill(Me.CustomersDataSet.Customers2)

        printDocument.DefaultPageSettings.Margins = New System.Drawing.Printing.Margins(40, 40, 40, 40)


        PrintProvider = Tools.PrintingDataGridViewProvider.Create(printDocument, _
                                                                  GridView, _
                                                                  chkLevelByLevel.Checked, _
                                                                  chkCenter.Checked, _
                                                                  chkFitColumns.Checked, _
                                                                  New Tools.TitlePrintBlock(printDocument.DocumentName, Color.DarkBlue), _
                                                                  New PrintsBlocks.HeaderPrintBlock(), _
                                                                  New PrintsBlocks.FooterPrintBlock())
  End Sub

    Private Sub chkCenter_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCenter.CheckedChanged
        If Not Me.PrintProvider Is Nothing Then
            PrintProvider.Drawer.MustCenterPartition = chkCenter.Checked
        End If

    End Sub

    Private Sub chkFitColumns_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFitColumns.CheckedChanged
        If Not Me.PrintProvider Is Nothing Then
            PrintProvider.Drawer.MustFitColumnsToPage = chkFitColumns.Checked
        End If
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If (printDialog.ShowDialog(Me) = Windows.Forms.DialogResult.OK) Then
            printDocument.Print()
        End If
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        If (printDialog.ShowDialog(Me) = DialogResult.OK) Then
            printPreviewDialog.ShowDialog(Me)
        End If
    End Sub

    Private Sub chkLevelByLevel_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkLevelByLevel.CheckedChanged
        If Not Me.PrintProvider Is Nothing Then
            Me.PrintProvider.Drawer.MustPrintLevelByLevel = chkLevelByLevel.Checked
        End If


    End Sub


    Private Sub GridView_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GridView.SelectionChanged
        chkSelectedRows.Enabled = GridView.SelectedRows.Count > 0

        If Not Me.PrintProvider Is Nothing Then
            PrintProvider.Drawer.MustPrintSelectedRows = _
                IIf(chkSelectedRows.Enabled, chkSelectedRows.Checked, False)
        End If


    End Sub

    Private Sub chkSelectedRows_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectedRows.CheckedChanged
        If Not Me.PrintProvider Is Nothing Then
            Me.PrintProvider.Drawer.MustPrintSelectedRows = chkSelectedRows.Checked
        End If
    End Sub

End Class
