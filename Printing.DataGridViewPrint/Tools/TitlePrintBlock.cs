﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Printing.DataGridViewPrint.Tools
{
    /// <author>Blaise Braye</author>
    /// <summary>
    /// Reusable Sample of a title to print on first printed sheet
    /// </summary>
    public class TitlePrintBlock : PrintBlock
    {

        /// <summary>
        /// title 
        /// </summary>
        public String Title { get; set; }

        /// <summary>
        /// color
        /// </summary>
        public Color ForeColor { get; set; }

        /// <summary>
        /// font
        /// </summary>
        public Font Font { get; set; }

        /// <summary>
        /// format
        /// </summary>
        public StringFormat Format { get; set; }

        /// <summary>
        /// title print block
        /// </summary>
        public TitlePrintBlock()
        {
            Format = new StringFormat();
            Format.LineAlignment = StringAlignment.Center;
            Format.Alignment = StringAlignment.Center;
            Format.Trimming = StringTrimming.Word;
            Format.FormatFlags = StringFormatFlags.LineLimit;
            this.Font = new Font("Tahoma", 14, FontStyle.Bold | FontStyle.Underline);
            this.Title = "this is a sample title";
            this.ForeColor = Color.Black;
        }


        /// <summary>
        /// title print block
        /// </summary>
        public TitlePrintBlock(String title)
            : this()
        {
            this.Title = title;
        }

        /// <summary>
        /// title print block
        /// </summary>
        public TitlePrintBlock(String title, Color foreColor)
            : this(title)
        {
            this.ForeColor = foreColor;
        }

        /// <summary>
        /// title print block
        /// </summary>
        public TitlePrintBlock(String title, Color foreColor, Font font)
            : this(title, foreColor)
        {
            this.Font = font;
        }

        /// <summary>
        /// title print block
        /// </summary>
        public TitlePrintBlock(String title, Color foreColor, Font font, StringFormat format)
            : this(title, foreColor, font)
        {
            this.Format = format;
        }


        /// <summary>
        /// get size
        /// </summary>
        public override SizeF GetSize(Graphics g, DocumentMetrics metrics)
        {
            return g.MeasureString(Title, Font, metrics.PrintAbleWidth, Format);
        }

        /// <summary>
        /// Draws a string.
        /// </summary>
        /// <param name="g"></param>
        /// <param name="codes"></param>
        public override void Draw(Graphics g, Dictionary<CodeEnum, string> codes)
        {
            g.DrawString(Title, Font, new SolidBrush(ForeColor), Rectangle, Format);
        }

    }
}
