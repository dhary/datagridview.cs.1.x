﻿
using System;
using System.Windows.Forms;
namespace Printing.DataGridViewPrint
{
    /// <author>Blaise Braye</author>
    /// <summary>
    /// Bounds of a partition, it covers mostly included rows and columns
    /// but also cordinates information about it (size,..)
    /// </summary>
    public class PartitionBounds
    {
        /// <summary>
        /// Starting row index
        /// </summary>
        public int StartRowIndex { get; set; }

        /// <summary>
        /// ending row index
        /// </summary>
        public int EndRowIndex { get; set; }

        /// <summary>
        /// Starting column index
        /// </summary>
        public int StartColumnIndex { get; set; }

        /// <summary>
        /// Ending column index
        /// </summary>
        public int EndColumnIndex { get; set; }

        /// <summary>
        /// Starting X position
        /// </summary>
        public float StartX { get; set; }

        /// <summary>
        /// Starting Y position
        /// </summary>
        public float StartY { get; set; }

        /// <summary>
        /// Partition Width
        /// </summary>
        public float Width { get; set; }

        /// <summary>
        /// Partition Height
        /// </summary>
        public float Height { get; set; }

        /// <summary>
        /// Row selector
        /// </summary>
        public Func<DataGridViewRow, bool> RowSelector { get; set; }

        /// <summary>
        /// Column selector
        /// </summary>
        public Func<DataGridViewColumn, bool> ColSelector { get; set; }

        /// <summary>
        /// Column count
        /// </summary>
        public int ColumnCount
        {
            get { return 1 + EndColumnIndex - StartColumnIndex; }
        }

        /// <summary>
        /// row count
        /// </summary>
        public int RowsCount
        {
            get { return 1 + EndRowIndex - StartRowIndex; }
        }
    }
}
